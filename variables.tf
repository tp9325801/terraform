variable "instance_name" {
  description = "Super TP Terraform AWS"
  type        = string
  default     = "MonInstanceEncorePlusRapide"
}
variable "region" {
  description = "AWS region for resources"
  type        = string
  default     = "eu-west-3"
}
variable "instance_type" {
  description = "EC2 instance type"
  type        = string
  default     = "t2.micro"
}
variable "access_key"{
  description = "Access key for AWS"
  type        = string
}
variable "secret_key"{
  description = "Secret key for AWS"
  type        = string
}
#Linux Ubuntu AMI ID
variable "ami_id" {
  type = string
  description = "The AMI to use"
  default = "ami-01d21b7be69801c2f"
}
variable "vpc_cidr_block" {
  type = string
  default = "192.168.1.0/24"
}
