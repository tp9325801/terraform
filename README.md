# TP Terraform Madjid Chabane

Prérequis :

- Installer [Terraform](https://registry.terraform.io/)
- Installer [AWS CLI](https://aws.amazon.com/fr/cli/)

1. Dans le fichier `terraform.tfvars`, mettre vos secret key et access key de votre compte AWS

2. Dans le fichier `variables.tf`, renseigner les variables à utiliser

3. Lancer un les commandes suivantes à la racine du dossier contenant les fichiers terraform

```
terraform init

terraform  validate

terraform plan

terraform apply
```

## Problèmes rencontrés

Le ami-id dans la documentation terraform correspond à une autre région, la section 'troubleshot' de cette même documentation permet de résoudre le problème.

Le nom du bucket doit être unique, mais les logs sont assez parlants.
